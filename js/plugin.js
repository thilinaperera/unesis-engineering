/**
 * Created by Thilina on 3/5/14.
 */
$featured = $('#featured');
if ( $featured.length ){
    et_slider_settings = {
        slideshow: false
    }

    if ( $featured.find('.slide').length == 1 ) et_slider_animate_items( $featured.find('.slide').eq(0) );

    if ( $featured.hasClass('et_slider_auto') ) {
        var et_slider_autospeed_class_value = /et_slider_speed_(\d+)/g;

        et_slider_settings.slideshow = true;

        et_slider_autospeed = et_slider_autospeed_class_value.exec( $featured.attr('class') );

        et_slider_settings.slideshowSpeed = et_slider_autospeed[1];
    }

    if ( $featured.hasClass('et_slider_effect_slide') ){
        et_slider_settings.animation = 'slide';
    }

    et_slider_settings.pauseOnHover = true;

    $featured.flexslider( et_slider_settings );
}


$(document).ready(function(){
    //Generates the captcha function
    var a = Math.ceil(Math.random() * 9)+ '';
    var b = Math.ceil(Math.random() * 9)+ '';
    var c = Math.ceil(Math.random() * 9)+ '';
    var d = Math.ceil(Math.random() * 9)+ '';
    var e = Math.ceil(Math.random() * 9)+ '';

    var code = a + b + c + d + e;

    $(".captchaCode").html(code);



    $('.formContact').validate({
        rules: {
            name: {
                required: true
            },
            email: {
                required: true
            },
            capcha:{
                required: true
            },
            phone:{
                required: true
            },
            message:{
                required: true
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

})

function validateCheck(){
    var str1 = removeSpaces($('.captchaCode').html());
    var str2 = removeSpaces($('#Captcha').val());
    if (str1 != str2){
        if(str2 == ""){

            alert("Security code should not be empty.");
            return false;
        }
        else{
            alert("Security code did not match.")
            return false;
        }

        return false;
    }
    return true

}
function removeSpaces(string){
    return string.split(' ').join('');
}

function checkform (){
    if($(".formContact").valid() && validateCheck()){
        return true;
    }
    else{
        return false;
    }
}


